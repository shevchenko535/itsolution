﻿using ITsolution.Model;
using ITsolution.Service;
using System.Collections.Generic;
using System.Linq;

namespace ITsolution.Web.ViewModels
{
    public class ProductViewModelBuilder
    {
        private readonly IProductService _productService;
        private readonly ICategoryService _categoryService;
        
        public ProductViewModelBuilder(IProductService productService, ICategoryService categoryService)
        {
            this._productService = productService;
            this._categoryService = categoryService;
        }

        public ProductViewModel Build() {
            return new ProductViewModel() { Categories = _GetAllCategories(new List<Category>()) };
        }

        public ProductViewModel Build(int id)
        {
            var product = _productService.GetProduct(id);
            var model = new ProductViewModel();
            if (product != null) {
                var productCategories = product.Categories;
                model.ProductID = product.ProductID;
                model.Name = product.Name;
                model.Description = product.Description;
                model.Link = product.Link;
                model.Categories = _GetAllCategories(productCategories);
            } 
            return model;
        }

        private List<CheckBoxListItem> _GetAllCategories(ICollection<Category> selectedCategories) {
            var allCategories = _categoryService.GetCategories();
            var checkBoxListItems = new List<CheckBoxListItem>();
            foreach (var category in allCategories)
            {
                checkBoxListItems.Add(new CheckBoxListItem()
                {
                    ID = category.CategoryID,
                    Display = category.Name,
                    IsChecked = selectedCategories.Where(x => x.CategoryID == category.CategoryID).Any()
                });
            }
            return checkBoxListItems;
        }

    }
}