﻿using ITsolution.Model;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace ITsolution.Web.ViewModels
{
    public class ProductViewModel
    {
        public int ProductID { get; set; }

        [DisplayName("Название: ")]
        [Required(ErrorMessage ="Вы не ввели название")]
        public string Name { get; set; }

        [DisplayName("Описание: ")]
        public string Description { get; set; }

        [DisplayName("URL: ")]
        [Url(ErrorMessage = "Неправильный формат ссылки")]
        public string Link { get; set; }

        [DisplayName("Категории: ")]
        public List<CheckBoxListItem> Categories { get; set; }

        public ProductViewModel()
        {
            Categories = new List<CheckBoxListItem>();
        }
    }
}