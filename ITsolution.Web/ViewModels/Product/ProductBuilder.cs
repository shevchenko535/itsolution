﻿using ITsolution.Model;
using ITsolution.Service;
using System.Collections.Generic;
using System.Linq;

namespace ITsolution.Web.ViewModels
{
    public class ProductBuilder
    {
        private readonly IProductService _productService;
        private readonly ICategoryService _categoryService;
        
        public ProductBuilder(IProductService productService, ICategoryService categoryService)
        {
            this._productService = productService;
            this._categoryService = categoryService;
        }

        public Product Build(ProductViewModel model) {

            List<int> selectedCategories = model.Categories.Where(x => x.IsChecked).Select(x => x.ID).ToList();
            var product = _productService.GetProduct(model.ProductID) ?? new Product();
            product.Name = model.Name;
            product.Link = model.Link;
            product.Description = model.Description;
            product.Categories.Clear();
            foreach (var categoryID in selectedCategories)
            {
                var category = _categoryService.GetCategory(categoryID);
                product.Categories.Add(category);
            }
            return product;
        }

    }
}