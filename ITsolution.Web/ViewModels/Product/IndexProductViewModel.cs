﻿using ITsolution.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace ITsolution.Web.ViewModels
{
    public class IndexProductViewModel
    {
        public int ProductID { get; set; }

        [DisplayName("Название")]
        public string Name { get; set; }

        [DisplayName("Описание")]
        public string Description { get; set; }

        [DisplayName("URL")]
        public string Link { get; set; }

        [DisplayName("Категории")]
        public string Categories { get; set; }
    }
}