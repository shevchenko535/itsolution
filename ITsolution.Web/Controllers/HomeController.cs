﻿using ITsolution.Service;
using ITsolution.Web.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace ITsolution.Web.Controllers
{
    public class HomeController : Controller
    {
        private readonly ICategoryService _categoryService;
        private readonly IProductService _productService;
        public HomeController(ICategoryService categoryService, IProductService productService)
        {
            this._categoryService = categoryService;
            this._productService = productService;
        }

        [HttpGet]
        public ActionResult Index()
        {
            var allProducts = _productService.GetProducts();

            var products = new List<IndexProductViewModel>();

            foreach (var product in allProducts)
            {
                var categories = String.Join(", ", product.Categories.Select(x => x.Name).ToList());
                products.Add(new IndexProductViewModel() { ProductID = product.ProductID, Name = product.Name, Link = product.Link, Description = product.Description, Categories = categories });
            }

            return View(products);
        }

        [HttpGet]
        public ActionResult Add()
        {
            var modelBuilder = new ProductViewModelBuilder(_productService, _categoryService);
            var model = modelBuilder.Build();
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Add(ProductViewModel model)
        {
            if (ModelState.IsValid)
            {
                var productBuilder = new ProductBuilder(_productService, _categoryService);
                var product = productBuilder.Build(model);
                _productService.CreateProduct(product);
                TempData["StatusMessage"] = "Продукт добавлен успешно.";
                return RedirectToAction("Index");
            }
            return View(model);
        }

        [HttpGet]
        public ActionResult Edit(int id)
        {

            var modelBuilder = new ProductViewModelBuilder(_productService, _categoryService);
            var model = modelBuilder.Build(id);
            if (model.ProductID != 0)
            {
                return View(model);
            }
            else {
                return RedirectToAction("Add");
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(ProductViewModel model)
        {
            if (ModelState.IsValid)
            {
                var productBuilder = new ProductBuilder(_productService, _categoryService);
                var product = productBuilder.Build(model);

                _productService.UpdateProduct(product);

                TempData["StatusMessage"] = "Продукт сохранен успешно.";
                return RedirectToAction("Index");
            }
            return View(model);
        }
    }
}