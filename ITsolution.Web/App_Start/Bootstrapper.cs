﻿using Autofac;
using Autofac.Integration.Mvc;
using ITsolution.Data.Infrastructure;
using ITsolution.Data.Repositories;
using ITsolution.Service;
using System.Linq;
using System.Reflection;
using System.Web.Mvc;

namespace ITsolution.Web.App_Start
{
    public static class Bootstrapper
    {
        public static void Run()
        {
            _SetAutofacContainer();
        }

        private static void _SetAutofacContainer()
        {
            var builder = new ContainerBuilder();
            builder.RegisterControllers(Assembly.GetExecutingAssembly());
            builder.RegisterType<UnitOfWork>().As<IUnitOfWork>().InstancePerRequest();
            builder.RegisterType<DbFactory>().As<IDbFactory>().InstancePerRequest();

            
            builder.RegisterAssemblyTypes(typeof(ProductRepository).Assembly)
                .Where(t => t.Name.EndsWith("Repository"))
                .AsImplementedInterfaces().InstancePerRequest();
            
            builder.RegisterAssemblyTypes(typeof(ProductService).Assembly)
               .Where(t => t.Name.EndsWith("Service"))
               .AsImplementedInterfaces().InstancePerRequest();

            IContainer container = builder.Build();
            DependencyResolver.SetResolver(new AutofacDependencyResolver(container));
        }
    }
}