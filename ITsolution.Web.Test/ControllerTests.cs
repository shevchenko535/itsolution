﻿using ITsolution.Model;
using ITsolution.Service;
using ITsolution.Web.Controllers;
using ITsolution.Web.ViewModels;
using Moq;
using NUnit.Framework;
using System.Collections.Generic;
using System.Web.Mvc;
using TestStack.FluentMVCTesting;
namespace ITsolution.Web.Test
{
    [TestFixture]
    class ControllerTests
    {
        private Mock<IProductService> _fakeProductService = new Mock<IProductService>();
        private Mock<ICategoryService> _fakeCategoryService = new Mock<ICategoryService>();

        [Test]
        public void Index_RenderView_ShouldBeDefaultView()
        {   
            // Arrange

            // Act 
            var sut = new HomeController(_fakeCategoryService.Object, _fakeProductService.Object);

            // Assert 
            sut.WithCallTo(x => x.Index()).ShouldRenderDefaultView();
        }

        [Test]
        public void Index_ShowProducts_ShouldShowFiveProducts()
        {
            // Arrange
            _fakeProductService.Setup(m => m.GetProducts()).Returns(StubData.GetProducts());
            var homeController = new HomeController(_fakeCategoryService.Object, _fakeProductService.Object);

            // Act 
            var sut = homeController.Index() as ViewResult;
            var viewModel = homeController.ViewData.Model as List<IndexProductViewModel>;

            // Assert 
            Assert.IsTrue(viewModel.Count == 5);

        }

        [Test]
        public void Add_RenderView_ShouldBeDefaultView()
        {
            // Arrange

            // Act 
            var sut = new HomeController(_fakeCategoryService.Object, _fakeProductService.Object);
            var result = sut.Add() as ViewResult;
            var viewModel = result.ViewData.Model as ProductViewModel;

            // Assert 
            sut.WithCallTo(x => x.Add()).ShouldRenderDefaultView();
            Assert.That(viewModel, Is.InstanceOf<ProductViewModel>());
        }

        [Test]
        public void Add_ExpectViewResult_ShouldReturnCorrectProductViewModel()
        {
            // Arrange
            var expectedViewModel = new ProductViewModel()
            {
                Categories = new List<CheckBoxListItem>() {
                    new CheckBoxListItem() {
                        ID = StubData.Category1.CategoryID,
                        Display = StubData.Category1.Name,
                        IsChecked = false
                    },
                    new CheckBoxListItem() {
                        ID = StubData.Category2.CategoryID,
                        Display = StubData.Category2.Name,
                        IsChecked = false
                    }
                }
            };
            _fakeCategoryService.Setup(m => m.GetCategories(null)).Returns(StubData.GetCategories());


            // Act 
            var homeController = new HomeController(_fakeCategoryService.Object, _fakeProductService.Object);
            var sut = homeController.Add() as ViewResult;
            var viewModel = homeController.ViewData.Model as ProductViewModel;

            // Assert 
            Assert.AreEqual(expectedViewModel.ProductID, viewModel.ProductID);
            Assert.AreEqual(expectedViewModel.Name, viewModel.Name);
            Assert.AreEqual(expectedViewModel.Link, viewModel.Link);
            Assert.AreEqual(expectedViewModel.Description, viewModel.Description);
            Assert.AreEqual(expectedViewModel.Categories[0].ID, viewModel.Categories[0].ID);
            Assert.AreEqual(expectedViewModel.Categories[0].Display, viewModel.Categories[0].Display);
            Assert.AreEqual(expectedViewModel.Categories[0].IsChecked, viewModel.Categories[0].IsChecked);
            Assert.AreEqual(expectedViewModel.Categories[1].ID, viewModel.Categories[1].ID);
            Assert.AreEqual(expectedViewModel.Categories[1].Display, viewModel.Categories[1].Display);
            Assert.AreEqual(expectedViewModel.Categories[1].IsChecked, viewModel.Categories[1].IsChecked);
        }


        [Test]
        public void Edit_RenderViewForExistingProduct_ShouldRenderDefaultView()
        {
            // Arrange
            _fakeProductService.Setup(m => m.GetProduct(It.IsAny<int>())).Returns(StubData.Product1);
            _fakeCategoryService.Setup(m => m.GetCategories(null)).Returns(StubData.GetCategories());

            // Act 
            var sut = new HomeController(_fakeCategoryService.Object, _fakeProductService.Object);
            var result = sut.Edit(1) as ViewResult;
            var viewModel = result.ViewData.Model as ProductViewModel;

            // Assert 
            sut.WithCallTo(x => x.Edit(1)).ShouldRenderDefaultView();
            Assert.That(viewModel, Is.InstanceOf<ProductViewModel>());
        }

        [Test]
        public void Edit_RenderViewForNotExistedProduct_ShouldRedirectToAddMethod()
        {
            // Arrange
            _fakeProductService.Setup(m => m.GetProduct(It.IsAny<int>())).Returns(new Product() { ProductID = 0, Name = "pr0" });
            _fakeCategoryService.Setup(m => m.GetCategories(null)).Returns(StubData.GetCategories());

            // Act 
            var sut = new HomeController(_fakeCategoryService.Object, _fakeProductService.Object);

            // Assert 
            sut.WithCallTo(x => x.Edit(0)).ShouldRedirectTo(x => x.Add);
        }

        [Test]
        public void Edit_ReturnViewResult_ShouldReturnProductViewModel()
        {
            // Arrange
            var expectedViewModel = new ProductViewModel()
            {
                ProductID = StubData.Product1.ProductID,
                Name = StubData.Product1.Name,
                Description = StubData.Product1.Description,
                Link = StubData.Product1.Link,
                Categories = new List<CheckBoxListItem>() {
                    new CheckBoxListItem() {
                        ID = StubData.Category1.CategoryID,
                        Display = StubData.Category1.Name,
                        IsChecked = true
                    },
                    new CheckBoxListItem() {
                        ID = StubData.Category2.CategoryID,
                        Display = StubData.Category2.Name,
                        IsChecked = false
                    }
                }
            };
            _fakeProductService.Setup(m => m.GetProduct(It.IsAny<int>())).Returns(StubData.Product1);
            _fakeCategoryService.Setup(m => m.GetCategories(null)).Returns(StubData.GetCategories());

            // Act 
            var homeController = new HomeController(_fakeCategoryService.Object, _fakeProductService.Object);
            var sut = homeController.Edit(1) as ViewResult;
            var viewModel = homeController.ViewData.Model as ProductViewModel;

            // Assert 
            Assert.AreEqual(expectedViewModel.ProductID, viewModel.ProductID);
            Assert.AreEqual(expectedViewModel.Name, viewModel.Name);
            Assert.AreEqual(expectedViewModel.Link, viewModel.Link);
            Assert.AreEqual(expectedViewModel.Description, viewModel.Description);
            Assert.AreEqual(expectedViewModel.Categories[0].ID, viewModel.Categories[0].ID);
            Assert.AreEqual(expectedViewModel.Categories[0].Display, viewModel.Categories[0].Display);
            Assert.AreEqual(expectedViewModel.Categories[0].IsChecked, viewModel.Categories[0].IsChecked);
            Assert.AreEqual(expectedViewModel.Categories[1].ID, viewModel.Categories[1].ID);
            Assert.AreEqual(expectedViewModel.Categories[1].Display, viewModel.Categories[1].Display);
            Assert.AreEqual(expectedViewModel.Categories[1].IsChecked, viewModel.Categories[1].IsChecked);
        }

    }
}
