﻿using ITsolution.Model;
using System.Collections.Generic;

namespace ITsolution.Web.Test
{
    class StubData
    {
        public static List<Product> GetProducts()
        {
            return new List<Product>() { Product1, Product2, Product3, Product4, Product5 };
        }

        internal static List<Category> GetCategories()
        {
            return new List<Category>() { Category1, Category2 };
        }

        public static Category Category1 = new Category { CategoryID = 1, Name = "Категория 1", Products = new List<Product>() };
        public static Category Category2 = new Category { CategoryID = 2, Name = "Категория 2", Products = new List<Product>() };

        public static Product Product1 = new Product { ProductID = 1, Name = "Продукт 1", Description = "Описание продукта 1", Link = "http://product1.com", Categories = new List<Category>() { Category1 } };
        public static Product Product2 = new Product { ProductID = 2, Name = "Продукт 2", Description = "Описание продукта 2", Link = "http://product2.com", Categories = new List<Category>() { Category1, Category2 } };
        public static Product Product3 = new Product { ProductID = 3, Name = "Продукт 3", Description = "Описание продукта 3", Link = "http://product3.com", Categories = new List<Category>() { Category1, Category2 } };
        public static Product Product4 = new Product { ProductID = 4, Name = "Продукт 4", Description = "Описание продукта 4", Link = "http://product4.com", Categories = new List<Category>() };
        public static Product Product5 = new Product { ProductID = 5, Name = "Продукт 5", Description = "Описание продукта 5", Link = "http://product5.com", Categories = new List<Category>() };
    }
}
