﻿using ITsolution.Model;
using ITsolution.Service;
using ITsolution.Web.ViewModels;
using Moq;
using NUnit.Framework;
using System.Collections.Generic;
using System.Linq;
namespace ITsolution.Web.Test
{
    [TestFixture]
    class BuilderTests
    {
        private Mock<IProductService> _fakeProductService = new Mock<IProductService>();
        private Mock<ICategoryService> _fakeCategoryService = new Mock<ICategoryService>();
     
        [Test]
        public void Build_ProductFromProductView_ShouldReturnCorrectProduct()
        {
            // Arrange
            var model = new ProductViewModel()
            {
                ProductID = StubData.Product1.ProductID,
                Name = StubData.Product1.Name,
                Description = StubData.Product1.Description,
                Link = StubData.Product1.Link,
                Categories = new List<CheckBoxListItem>() {
                    new CheckBoxListItem() {
                        ID = StubData.Category1.CategoryID,
                        Display = StubData.Category1.Name,
                        IsChecked = true
                    },
                    new CheckBoxListItem() {
                        ID = StubData.Category2.CategoryID,
                        Display = StubData.Category2.Name,
                        IsChecked = false
                    }
                }
            };
            var expectedProduct = StubData.Product1;

            _fakeProductService.Setup(m => m.GetProduct(It.IsAny<int>())).Returns(StubData.Product1);
            _fakeCategoryService.Setup(m => m.GetCategories(null)).Returns(StubData.GetCategories());
            _fakeCategoryService.Setup(m => m.GetCategory(1)).Returns(StubData.Category1);
            _fakeCategoryService.Setup(m => m.GetCategory(2)).Returns(StubData.Category2);

            // Act 
            var productBuilder = new ProductBuilder(_fakeProductService.Object, _fakeCategoryService.Object);
            var sut = productBuilder.Build(model);


            // Assert 
            Assert.AreEqual(expectedProduct.ProductID, sut.ProductID);
            Assert.AreEqual(expectedProduct.Name, sut.Name);
            Assert.AreEqual(expectedProduct.Link, sut.Link);
            Assert.AreEqual(expectedProduct.Description, sut.Description);
            Assert.IsTrue(expectedProduct.Categories.Count == 1);
            Assert.AreEqual(expectedProduct.Categories.ToList()[0].CategoryID, sut.Categories.ToList()[0].CategoryID);
            Assert.AreEqual(expectedProduct.Categories.ToList()[0].Name, sut.Categories.ToList()[0].Name);

        }

        [Test]
        public void Build_ProductViewFromExistingProduct_ShouldReturnProductView()
        {
            // Arrange
            var expectedModel = new ProductViewModel()
            {
                ProductID = StubData.Product1.ProductID,
                Name = StubData.Product1.Name,
                Description = StubData.Product1.Description,
                Link = StubData.Product1.Link,
                Categories = new List<CheckBoxListItem>() {
                    new CheckBoxListItem() {
                        ID = StubData.Category1.CategoryID,
                        Display = StubData.Category1.Name,
                        IsChecked = true
                    },
                    new CheckBoxListItem() {
                        ID = StubData.Category2.CategoryID,
                        Display = StubData.Category2.Name,
                        IsChecked = false
                    }
                }
            };


            _fakeProductService.Setup(m => m.GetProduct(It.IsAny<int>())).Returns(StubData.Product1);
            _fakeCategoryService.Setup(m => m.GetCategories(null)).Returns(StubData.GetCategories());

            // Act 
            var productViewBuilder = new ProductViewModelBuilder(_fakeProductService.Object, _fakeCategoryService.Object);
            var sut = productViewBuilder.Build(1);


            // Assert 
            Assert.AreEqual(expectedModel.ProductID, sut.ProductID);
            Assert.AreEqual(expectedModel.Name, sut.Name);
            Assert.AreEqual(expectedModel.Link, sut.Link);
            Assert.AreEqual(expectedModel.Description, sut.Description);
            Assert.IsTrue(expectedModel.Categories.Count == 2);
            Assert.AreEqual(expectedModel.Categories[0].ID, sut.Categories[0].ID);
            Assert.AreEqual(expectedModel.Categories[0].Display, sut.Categories[0].Display);
            Assert.AreEqual(expectedModel.Categories[0].IsChecked, sut.Categories[0].IsChecked);
            Assert.AreEqual(expectedModel.Categories[1].ID, sut.Categories[1].ID);
            Assert.AreEqual(expectedModel.Categories[1].Display, sut.Categories[1].Display);
            Assert.AreEqual(expectedModel.Categories[1].IsChecked, sut.Categories[1].IsChecked);
        }

        [Test]
        public void Build_ProductViewFromNotExistedProduct_ShouldReturnEmptyProductView()
        {
            // Arrange
            var expectedModel = new ProductViewModel()
            {
                Categories = new List<CheckBoxListItem>() {
                    new CheckBoxListItem() {
                        ID = StubData.Category1.CategoryID,
                        Display = StubData.Category1.Name,
                        IsChecked = false
                    },
                    new CheckBoxListItem() {
                        ID = StubData.Category2.CategoryID,
                        Display = StubData.Category2.Name,
                        IsChecked = false
                    }
                }
            };


            _fakeProductService.Setup(m => m.GetProduct(It.IsAny<int>())).Returns((Product)null);
            _fakeCategoryService.Setup(m => m.GetCategories(null)).Returns(StubData.GetCategories());


            // Act 
            var productViewBuilder = new ProductViewModelBuilder(_fakeProductService.Object, _fakeCategoryService.Object);
            var sut = productViewBuilder.Build();

            // Assert 
            Assert.AreEqual(expectedModel.ProductID, sut.ProductID);
            Assert.AreEqual(expectedModel.Name, sut.Name);
            Assert.AreEqual(expectedModel.Link, sut.Link);
            Assert.AreEqual(expectedModel.Description, sut.Description);
            Assert.IsTrue(expectedModel.Categories.Count == 2);
            Assert.AreEqual(expectedModel.Categories[0].ID, sut.Categories[0].ID);
            Assert.AreEqual(expectedModel.Categories[0].Display, sut.Categories[0].Display);
            Assert.AreEqual(expectedModel.Categories[0].IsChecked, sut.Categories[0].IsChecked);
            Assert.AreEqual(expectedModel.Categories[1].ID, sut.Categories[1].ID);
            Assert.AreEqual(expectedModel.Categories[1].Display, sut.Categories[1].Display);
            Assert.AreEqual(expectedModel.Categories[1].IsChecked, sut.Categories[1].IsChecked);
        }

    }
}
