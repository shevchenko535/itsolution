﻿using System.Collections.Generic;

namespace ITsolution.Model
{
    public class Category
    {
        public Category()
        {
            this.Products = new HashSet<Product>();
        }
        public int CategoryID { get; set; }
        
        public string Name { get; set; }

        public virtual ICollection<Product> Products { get; set; }
    }
}