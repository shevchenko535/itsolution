﻿using System;
using System.Collections.Generic;

namespace ITsolution.Model
{
    public class Product
    {
        public Product()
        {
            DateCreated = DateTime.Now;
            this.Categories = new HashSet<Category>();
        }
        public int ProductID { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Link { get; set; }
        public ICollection<Category> Categories { get; set; }
        public DateTime? DateCreated { get; set; }
        public DateTime? DateUpdated { get; set; }

    }
}