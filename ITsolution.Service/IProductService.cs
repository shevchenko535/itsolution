﻿using System.Collections.Generic;
using ITsolution.Model;

namespace ITsolution.Service
{
    public interface IProductService
    {
        void CreateProduct(Product product);
        Product GetProduct(int id);
        IEnumerable<Product> GetProducts();
        void SaveProduct();
        void UpdateProduct(Product product);
    }
}