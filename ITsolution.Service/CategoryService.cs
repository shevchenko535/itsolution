﻿using ITsolution.Data.Infrastructure;
using ITsolution.Data.Repositories;
using ITsolution.Model;
using System.Collections.Generic;
using System.Linq;

namespace ITsolution.Service
{
    public class CategoryService : ICategoryService
    {
        private readonly ICategoryRepository _categorysRepository;
        private readonly IUnitOfWork _unitOfWork;

        public CategoryService(ICategoryRepository categorysRepository, IUnitOfWork unitOfWork)
        {
            this._categorysRepository = categorysRepository;
            this._unitOfWork = unitOfWork;
        }

        #region ICategoryService Members

        public IEnumerable<Category> GetCategories(string name = null)
        {
            if (string.IsNullOrEmpty(name))
                return _categorysRepository.GetAll();
            else
                return _categorysRepository.GetMany(c => c.Name == name);  
        }

        public Category GetCategory(int id)
        {
            return _categorysRepository.GetById(id);
        }

        public Category GetCategory(string name)
        {
            return _categorysRepository.GetCategoryByName(name);
        }
    
        #endregion
    }
}
