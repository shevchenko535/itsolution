﻿using System.Collections.Generic;
using ITsolution.Model;

namespace ITsolution.Service
{
    public interface ICategoryService
    {
        IEnumerable<Category> GetCategories(string name = null);
        Category GetCategory(string name);
        Category GetCategory(int id);
    }
}