﻿using ITsolution.Data.Infrastructure;
using ITsolution.Data.Repositories;
using ITsolution.Model;
using System.Collections.Generic;

namespace ITsolution.Service
{

    public class ProductService : IProductService
    {
        private readonly IProductRepository _productsRepository;
        private readonly ICategoryRepository _categoryRepository;
        private readonly IUnitOfWork _unitOfWork;

        public ProductService(IProductRepository productsRepository, ICategoryRepository categoryRepository, IUnitOfWork unitOfWork)
        {
            this._productsRepository = productsRepository;
            this._categoryRepository = categoryRepository;
            this._unitOfWork = unitOfWork;
        }

        #region IProductService Members

        public IEnumerable<Product> GetProducts()
        {
            return _productsRepository.GetAll();
        }

        public Product GetProduct(int id)
        {
            return _productsRepository.GetByIdWithCategories(id);
        }

        public void CreateProduct(Product product)
        {
            _productsRepository.Add(product);
            SaveProduct();
        }

        public void UpdateProduct(Product product)
        {
            _productsRepository.Update(product);
            SaveProduct();
        }

        public void SaveProduct()
        {
            _unitOfWork.Commit();
        }

        #endregion
    }
}
