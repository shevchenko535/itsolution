﻿using ITsolution.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ITsolution.Data
{
    public class ITsolutionSeedData : DropCreateDatabaseIfModelChanges<ITsolutionEntities>
    {
        protected override void Seed(ITsolutionEntities context)
        {
            
            var cat1 = new Category { Name = "Категория 1", Products = new List<Product>() };
            var cat2 = new Category { Name = "Категория 2", Products = new List<Product>() };
            var cat3 = new Category { Name = "Категория 3", Products = new List<Product>() };
            var cat4 = new Category { Name = "Категория 4", Products = new List<Product>() };
            var cat5 = new Category { Name = "Категория 5", Products = new List<Product>() };

            var prod1 = new Product { Name = "Продукт 1", Description = "Описание продукта 1", Link = "http://product1.com", Categories = new List<Category>() };
            var prod2 = new Product { Name = "Продукт 2", Description = "Описание продукта 2", Link = "http://product2.com", Categories = new List<Category>() };
            var prod3 = new Product { Name = "Продукт 3", Description = "Описание продукта 3", Link = "http://product3.com", Categories = new List<Category>() };
            var prod4 = new Product { Name = "Продукт 4", Description = "Описание продукта 4", Link = "http://product4.com", Categories = new List<Category>() };
            var prod5 = new Product { Name = "Продукт 5", Description = "Описание продукта 5", Link = "http://product5.com", Categories = new List<Category>() };

            prod1.Categories.Add(cat1);

            prod2.Categories.Add(cat1);
            prod2.Categories.Add(cat2);

            prod3.Categories.Add(cat1);
            prod3.Categories.Add(cat2);
            prod3.Categories.Add(cat3);
            prod3.Categories.Add(cat4);


            context.Products.Add(prod1);
            context.Products.Add(prod2);
            context.Products.Add(prod3);
            context.Products.Add(prod4);
            context.Products.Add(prod5);
            
            context.Categories.Add(cat1);
            context.Categories.Add(cat2);
            context.Categories.Add(cat3);
            context.Categories.Add(cat4);
            context.Categories.Add(cat5);

            context.Commit();
        }


    }
}
