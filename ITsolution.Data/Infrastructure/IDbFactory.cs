﻿using System;

namespace ITsolution.Data.Infrastructure
{
    public interface IDbFactory : IDisposable
    {
        ITsolutionEntities Init();
    }
}
