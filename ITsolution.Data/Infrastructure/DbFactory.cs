﻿namespace ITsolution.Data.Infrastructure
{
    public class DbFactory : Disposable, IDbFactory
    {
        ITsolutionEntities dbContext;

        public ITsolutionEntities Init()
        {
            return dbContext ?? (dbContext = new ITsolutionEntities());
        }

        protected override void DisposeCore()
        {
            if (dbContext != null)
                dbContext.Dispose();
        }
    }
}
