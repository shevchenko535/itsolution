﻿using ITsolution.Data.Infrastructure;
using ITsolution.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;

namespace ITsolution.Data.Repositories
{
    public interface IProductRepository : IRepository<Product>
    {
        Product GetByIdWithCategories(int id);
    }
}
