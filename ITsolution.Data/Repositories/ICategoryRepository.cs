﻿using ITsolution.Data.Infrastructure;
using ITsolution.Model;

namespace ITsolution.Data.Repositories
{
    public interface ICategoryRepository : IRepository<Category>
    {
        Category GetCategoryByName(string categoryName);
    }
}