﻿using ITsolution.Data.Infrastructure;
using ITsolution.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;

namespace ITsolution.Data.Repositories
{
    public class ProductRepository : RepositoryBase<Product>, IProductRepository
    {
        public ProductRepository(IDbFactory dbFactory)
            : base(dbFactory) { }


        public override IEnumerable<Product> GetAll() {
            var products = this.DbContext.Products.Include(c => c.Categories).ToList();
            return products;
        }

        public Product GetByIdWithCategories(int id)
        {
            var product = this.DbContext.Products.Include(c => c.Categories).SingleOrDefault(p => p.ProductID == id); ;
            return product;
        }

        public override void Update(Product entity)
        {
            entity.DateUpdated = DateTime.Now;
            base.Update(entity);
        }
    }
}
