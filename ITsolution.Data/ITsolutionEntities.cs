﻿using ITsolution.Model;
using System.Data.Entity;

namespace ITsolution.Data
{
    public class ITsolutionEntities : DbContext
    {
        public ITsolutionEntities() : base("ITsolutionEntities") { }

        public DbSet<Product> Products { get; set; }
        public DbSet<Category> Categories { get; set; }

        public virtual void Commit()
        {
            base.SaveChanges();
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
        }
    }
}
